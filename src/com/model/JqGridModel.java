package com.model;

public class JqGridModel {
	private int id;
	private String firstName;
	private String lastName;
	private String city;
	private String state;
	
	public int getId() {
		return id;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public String getCity() {
		return city;
	}
	
	public String getState() {
		return state;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	/**
	For jQGrid, page property represents current page number, and total represent total no pages, records represents Total number of records and rows represent the actual data. A sample return value from the above servlet is shown below
	 
	{“page”:1,”total”:”2″,”records”:2,”rows”:[
	{
	“id”: 1,
	“firstName”: “Mohaideen”,
	“lastName”: “Jamil”,
	“city”: “Coimbatore”,
	“state”: “TamilNadu”
	},
	{
	“id”: 2,
	“firstName”: “Ameerkhan”,
	“lastName”: “Saffar”,
	“city”: “Thirunelveli”,
	“state”: “Tamilnadu”
	}
	]}
	* */
}
