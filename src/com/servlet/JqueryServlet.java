package com.servlet;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class JqueryServlet
 */
@WebServlet("/JqueryServlet")
public class JqueryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public JqueryServlet() {
		super();
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//context of app
		ServletContext context = this.getServletContext();
		ServletContext servletContext = this.getServletConfig().getServletContext();
		
		String username = context.getInitParameter("username");
		String password = context.getInitParameter("password");
		
		//config of servlet
		ServletConfig config = this.getServletConfig();
		String stt = config.getInitParameter("stt");
		
		
		String userName = request.getParameter("userName");
		if (userName.equals("")) {
			userName = "User name cannot be empty";
		} else {
			userName = "Hello " + userName;
		}
		response.setContentType("text/plain");
		response.getWriter().write(userName);
		response.getWriter().write("Hello: " + username + "-" + password);
		response.getWriter().write(stt);
		
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}
	
}
