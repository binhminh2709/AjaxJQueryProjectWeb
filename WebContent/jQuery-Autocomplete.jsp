<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>jQuery-Autocomplete</title>
<script type="text/javascript" src="js/jQuery-Autocomplete/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="js/jQuery-Autocomplete/jquery.mockjax.js"></script>
<!-- 
<script type="text/javascript" src="js/jQuery-Autocomplete/countries.js"></script>
 -->
<script type="text/javascript" src="js/jQuery-Autocomplete/dmShipAgencies.js"></script>
<script type="text/javascript" src="js/jQuery-Autocomplete/findDmShipAgencies.js"></script>
<script type="text/javascript" src="js/jQuery-Autocomplete/jquery.autocomplete.js"></script>
<style type="text/css">

 .autocomplete-suggestions {
    position: absolute;
    max-height: 300px;
    z-index: 9999;
    top: 121.8125px;
    left: 8px;
    width: 571px !important;
    display: block;
    overflow-x: scroll !important;
    background-color: antiquewhite;
    
    color: #fff;
    padding: 1px;
    color: inherit;
    font: inherit;
    margin: 0;
  }
  
  .autocomplete-selected {
    background-color: #065490;
  }
  
  .autocomplete-ajax{position: absolute; z-index: 2; background: transparent;}
  .autocomplete-ajax-x{color: #CCC; position: absolute; background: transparent; z-index: 1;}
  
 
</style>
</head>
<body>
  <h2>Ajax Lookup</h2>
  <p>Type country name in english:</p>
  <div style="position: relative; height: 300px;">
    <input type="text" name="country" id="autocomplete-ajax" style="width: 500px;"/>
    <input type="text" name="country" id="autocomplete-ajax-x" disabled="disabled"/>
    <input type="text" name="id-selction-ajax" id="id-selction-ajax" value="" readonly="readonly"/>
  </div>
  <div id="selction-ajax"></div>
  
  <div style="position: relative; height: 80px;">
    <input type="text" name="autocomplete" id="autocomplete" style="width: 500px;">
  </div>
</body>
</html>