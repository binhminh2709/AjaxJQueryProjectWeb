<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Pie Chart YUI3</title>
<link rel="stylesheet" href="css/styleyui3.css">
<!-- 
<script src="js/yui/yui-min.js" type="text/javascript"></script>
 -->
<script src="http://yui.yahooapis.com/3.8.1/build/yui/yui-min.js"></script>
<script>
	YUI().use('charts', 'charts-legend', 'io', function m(Y) {
	    var data, urifordata = "./PieChartServlet", marklinechart = Y.one("#pieChartId"), configuration = {
	        method : 'POST',
	        headers : {
		        'Content-Type' : 'application/json',
	        },
	        
	        on : {
	            success : function(transactionid, response, arguments) {
		            data = JSON.parse(response.responseText), pieChart = new Y.Chart({
		                type : "pie",
		                stacked : true,
		                dataProvider : data,
		                categoryKey : 'Source',
		                legend : {
		                    position : "right",
		                    width : 100,
		                    height : 100,
		                },
		                render : marklinechart,
		            });
	            },
	            
	            failure : function(transactionid, response, arguments) {
		            alert("Error In Data Loading.");
	            }
	        }
	    };
	    Y.io(urifordata, configuration);
    });
</script>
<style>
#pieChartId {
	height: 400px;
	margin: 10px;
	max-width: 500px;
	width: 90%;
}
</style>
</head>
<body>
	<div class="wrapper">
		<div class="container">
			<div class="header">
				<h3>Pie Chart Demo using YUI3 Charts in Java web application</h3>
			</div>
			<br />
			<div id="pieChartId"></div>
		</div>
	</div>
</body>
</html>