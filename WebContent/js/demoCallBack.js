$(document).ready(function() {

//A1 = function(data, callback) {
//	$a.jax() 
//	success: If(callback) {
//		callback(); 
//	}
//};

//Note that the item in the click method's parameter is a function, not a variable.​
//The item is a callback function
//hai ham function, ham function thu 2 moi chay, loi, callback sang function 1
$('#btn_1').click(console.log('hello'), function(){
	$('p').hide("slow", function(){
		alert("The paragraph is now hidden");
	})
});



var friends = ["Mike", "Stacy", "Andy", "Rick"];

friends.forEach(function (eachName, index){
	console.log(index + 1 + ". " + eachName); // 1. Mike, 2. Stacy, 3. Andy, 4. Rick​
})


// global variable​
var allUserData = [];

// generic logStuff function that prints to console​
function logStuff (userData) {
    if ( typeof userData === "string") {
        console.log(userData);
    } else if ( typeof userData === "object") {
        for (var item in userData) {
            console.log(item + ": " + userData[item]);
        }
    }
}

// A function that takes two parameters, the last one a callback function​
function getInput (options, callback) {
    allUserData.push (options);
    callback (options);
}

// When we call the getInput function, we pass logStuff as a parameter.​
// So logStuff will be the function that will called back (or executed) inside the getInput function​
getInput ({name:"Rich", speciality:"JavaScript"}, logStuff);
//  name: Rich​
// speciality: JavaScript

});