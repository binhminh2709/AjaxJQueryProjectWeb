$(function() {
	$("#form").validate({
		rules : {
			name : {
				required : true,
				minlength : 4,
				maxlength : 20,
			}
		},
		messages : {
			name : {
				required : "Please enter a name",
				//minlength : $.format("Minimum {0} characters required!"),
				//maxlength : $.format("Maximum {0} characters allowed!")
				minlength : jQuery.validator.format("Minimum {0} characters required!"),
				maxlength : jQuery.validator.format("Maximum {0} characters allowed!")
			}
		}
	});
	
	/**
	The two parameters we used in validate method are:
	 
		rules: allows you to specify which fields you want to validate.
		messages: allows you to specify the error message for a particular field. If you don’t specify
	it, a default message is provided that says “this field is required”.
	 
		In the example above, we only used three validation methods (required, minlength and maxlength).
	I’ve also introduced the format method in the message. This lets us replace the argument given
	for minlength and maxlength, such that we don’t need to hard-code that value if they ever change.
	 
		There are several other methods that can be used here. You can find list of built-in validation
	methods at Validation Methods.
	 */
	
	 $("#form").validate({
		rules : {
			name : {
				customvalidation : true
			}
		}
	});
	$.validator.addMethod("customvalidation", function(value, element) {
		return /^[A-Za-z_ -]+$/.test(value);
	}, "Alpha Characters Only.");
	
/**
	Here I have defined our own validation method by calling the jQuery.validator.addMethod() method.
This method takes three parameters:
	- name: The name of the method, used to identify and referencing it, must be a valid
javascript identifier.
	- method: the actual method implementation, returning true if an element is valid.
	- message: The default message to display for this method.
 * */
});
