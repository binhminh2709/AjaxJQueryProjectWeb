performRequestToFM: function(request) {
	return $.ajax({
	    url: configServiceUri + request.id,
	    data: request.param,
	    type: request.type,
	    dataType: 'json',
	    contentType: 'application/json',
	    cache: false,
	    async: true,
	    xhrFields: {withCredentials: true},
	    error: function(xhr, errDesc) {
	        log.error(configServiceUri + request.id + " - CODE: " + xhr.status + "-" + errDesc);
	        if (xhr.status == 503 || errDesc == "timeout") { // Error: Service Unavailable
	            Models.PopupManagement.showMessageBox({
	                theTitle: Common.getTranslate(4307),
	                idObject: "popupErrorConnection",
	                theMessage: Common.getTranslate(4331),
	                onOK: function() {
	                },
	                onCancel: function() {
	                }
	            });
	            MmiProxy.requestQueue.requests = new Array();
	        } else {
	            MmiProxy.triggerNextRequest();
	        }
	        MmiProxy.requestQueue.locked = false;
	        if (request.callback) {
	            request.callback();
			}
	    },
        success: function(data, status) {
            log.debug(configServiceUri + request.id);
            MmiProxy.requestQueue.locked = false;
            MmiProxy.triggerNextRequest();

            if (request.callback) {
                if (request.type == "GET") {
                    request.callback(data, status);
                } else {
                    request.callback(status, data);
                }
            }
        }
	});
},

//sau có 1 hàm gọi tới kiểu này

Models.MmiProxy.getValueById(MMI.DATA.RATE_ZIP_CODE_AVAILABLE, function(data, status) {
	var aZipcodeAvailabel = false;
	if (data) {
	    aZipcodeAvailabel = data.value;
	}
	if (callback) {
	    callback(aZipcodeAvailabel);
	}
});
//gọi callback kiểu này

init: function(element, options) {
    if (!JobList.prototype.isLoadedJob) {
        JobList.prototype.isLoadedJob = true;
        Common.windowID = WINDOW_ID.JOBLIST;
        Models.PopupManagement.showLoadingView({
            theTitle: Common.getTranslate(3776),
            theMessage: Common.getTranslate(3697)
        });
        Grid_User.prototype.validActionLogin = false;
        Models.JobList.prototype.initJobTypetLib();
        Models.JobList.prototype.reloadJobList(function() {
            JobList.prototype.reloadJobsData();
            Models.UserLogin.Account.initAccountsArray(function() {
                Models.JobList.prototype.checkExistEldMode(function() {
                    can.each(Models.JobList.jobArray, function(item, index) {
                        JobList.prototype.checkParametersMissing(item);
                    });
                    setTimeout(function() {
                        Models.JobList.prototype.initInkInformation(function() {
                            Models.JobList.prototype.checkAvailableEKP(function() {
                                Models.JobList.prototype.initFundsFromFM(function() {
                                    Monitor.prototype.lastConnectUri = configServiceUri;
                                    options.jobList = Models.JobList.jobArray;
                                    $("#main_content").append(can.view(options.view, options));
                                    JobList.prototype.initScrollList();
                                    JobList.prototype.initSortable();
                                    Common.addScrollbar("#scroll-container", true);
                                    $("#scroll-container .mCSB_scrollTools").css('height', $("#scroll-container").css("height"));
                                    Models.JobList.prototype.JobMenu(function(jobsMenu) {
                                        Models.Settings.PPISetting.reloadJobPPI();
                                        $("#addnew_button").bind("click", function() {
                                            JobList.prototype.addnewButtonClick(jobsMenu);
                                        });
                                        Models.JobList.prototype.getFractionalStatus(function() {
                                            Models.JobList.prototype.getStandardCounter(function() {
                                                JobList.prototype.isLoadedJob = false;
                                                Models.MmiProxy.setValueById(MMI.DATA.IMPRINT_PRINTING_MODE, 0, function() {
                                                    Models.MmiProxy.startActionById(MMI.ACTION.START_NEW_STAMP_SELECTION, function(status) {
                                                        if (!countryConfiguration.feature.hasQRCode) {
                                                            Models.Settings.SloganSettingModel.downloadSloganByIndexFromFM(0);
                                                            if (typeof(Models.Settings.TextSettingModel.arrayFreeText[0]) != "undefined" && typeof(Models.Settings.TextSettingModel.arrayFreeText[0].id) != "undefined") {
                                                                Models.Settings.TextSettingModel.getLinesByIdFromFM(Models.Settings.TextSettingModel.arrayFreeText[0].id);
                                                            }
                                                        }
                                                        if (Compatibility.isJobPermit && Models.Settings.PPISetting.PPIArray.length) {
                                                            Models.Settings.PPISetting.downloadPPIByIdFromFM(Models.Settings.PPISetting.PPIArray[0].id);
                                                        }
                                                        Models.PopupManagement.hideLoadingView();
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    }, 300);
                });
            });
        });
    }
}