$(document).ready(function() {
	$('#StudentTableContainer').jtable({
		title : 'Students List',
		
		paging : true, //Set paging enabled
		pageSize : 3, //Set page size, sets the initial number of records to be displayed per page.
		
		actions : {
			listAction   : 'Controller?action=list',
			createAction : 'Controller?action=create',
			updateAction : 'Controller?action=update',
			deleteAction : 'Controller?action=delete'
		},
		fields : {
			studentId : {
				title : 'Student Id',
				width : '10%',
				key : true,
				list : true,
				edit : false,
				create : true
			},
			studentName : {
				title : 'Student Name',
				width : '30%',
				edit : true
			},
			department : {
				title : 'Department',
				width : '30%',
				edit : true
			},
			emailId : {
				title : 'Email',
				width : '30%',
				edit : true
			}
		}
	});
	$('#StudentTableContainer').jtable('load');
});