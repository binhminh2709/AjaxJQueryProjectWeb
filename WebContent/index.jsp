<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>AJAX-Jquery-minhnd</title>
<script src="js/jquery-1.11.2.min.js" type="text/javascript"></script>
<script src="js/jquery-ui.min.js" type="text/javascript"></script>
<script src="js/ajax.js" type="text/javascript"></script>
<script>
	$(document).ready(function() {

		$('#sports').change(function(event) {
			var sports = $("select#sports").val();
			$.get('JsonServlet', {
				sportsName : sports
			}, function(response) {
				
				var select = $('#player');
				select.find('option').remove();
				$.each(response, function(index, value) {
					$('<option>').val(value).text(value).appendTo(select);
				});
			});
		});
	});
</script>
</head>
<body>

<!-- AJAX implementation in JSP and Servlet using JQuery -->
	<form>
		<fieldset>
			<legend>AJAX implementation in JSP and Servlet using JQuery</legend>
			<br /> Enter your Name: <input type="text" id="userName" />
		</fieldset>

		<fieldset>
			<legend>Response from jQuery Ajax Request on Blur event</legend>
			<div id="ajaxResponse"></div>
		</fieldset>
	</form>
	<br><br>
	
<!-- Dynamic Dependent Select Box in JSP & Servlet using JQuery and JSON via Ajax -->
	<form>
		<fieldset>
		  <legend>Dynamic Dependent Select Box in JSP & Servlet using JQuery and JSON via Ajax</legend>
			Select Favorite Sports: <select
				id="sports">
				<option>Select Sports</option>
				<option value="Football">Football</option>
				<option value="Cricket">Cricket</option>
			</select> <br /> <br /> Select Favorite Player:
			<select id="player">
				<option>Select Player</option>
			</select>
		</fieldset>
	</form>
	<br><br>

	<ul>
		<li><a href="jtable.jsp">CRUD operations using jTable in J2EE</a></li>
		<li><a href="uploadFile.jsp">Ajax File Upload with Progress Bar using jQuery in Java web application</a></li>
		<li><a href="validationPlugin.jsp">jQuery form validation using jQuery Validation plugin</a></li>
		<li><a href="jqGrid.jsp">Setup and Load Data in jQGrid using Servlets and JSP</a></li>
		<li><a href="autoComplete.jsp">Autocomplete in java web application using Jquery and JSON</a></li>
		<li><a href="tabStyleLoginAndSignup.jsp">Tab Style Login and Signup example using jQuery in Java web application</a></li>
		<li><a href="gridView.jsp">Gridview in Servlets using jQuery DataTables plugin</a></li>
		<li><a href="pieChart.jsp">Pie Chart using YUI3 jquery chart plugin in Java web application</a></li>
		
		<li><a href="auto-RefreshingChart.jsp">Auto-Refreshing Pie Chart/Bar Chart in Servlet dynamically using JFreeChart</a></li>
		<li><a href="demoCallBack.jsp">Understand JavaScript Callback Functions and Use Them</a></li>
		<li><a href=""></a></li>
		<li><a href=""></a></li>
		
	</ul>
  

  

</body>
</html>

<!--
Here when the user types a name in the “userName textbox” and click anywhere outside the textbox,
then its blur event gets triggered and the ‘get’ function executes the Ajax GET request on the Servlet.
Here the first argument of get method is the URL, second argument is a key-value pair that passes
the parameter from JSP to Servlet and the third argument is a function that defines
what is to be done with the response that is got back from the Servlet.
 
Note :
This get method is a shorthand Ajax function, which is equivalent to:
$.ajax({
  url: url,
  data: data,
  success: success,
  dataType: dataType
});
 -->