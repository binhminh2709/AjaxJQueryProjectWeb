<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>TabStyleLoginAndSignup</title>
<link rel="stylesheet" href="css/jquery-ui.min.css">
<link rel="stylesheet" href="css/styleTab.css">
<script src="js/jquery-1.11.2.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script>
	$(function() {
	    $("#tabs").tabs();
    });
</script>
</head>
<body>
	<div class="wrapper">
		<div class="container">
			<div id="tabs">
				<ul>
					<li><a href="#login">Login</a></li>
					<li><a href="#register">Sign up</a></li>
				</ul>
				<div id="login">
					<%
						if ("Invalid Email or password".equals((String) session.getAttribute("error"))) {
					%>
					<h6>Invalid Email or password. Please try again.</h6>
					<%
						}
					%>
					<form method="post" action="LoginController">
						<label for="email">Email:</label> <br /> <input type="text"
							name="email" id="email" /> <br /> <label for="password">Password:</label>
						<br /> <input type="password" name="password" id="password" /> <br />
						<br /> <input type="submit" value="Login">
					</form>
				</div>
				<div id="register">
					<form method="post" action="RegistrationController">
						<label for="fullname">Name:</label><br /> <input type="text"
							name="fullname" id="fullname" /> <br /> <label for="email">Email:</label><br />
						<input type="text" name="email" id="email" /> <br /> <label
							for="password">Password:</label><br /> <input type="password"
							name="password" id="password" /> <br /> <label for="gender">Gender:</label><br />
						<select name="gender" id="gender">
							<option value="Male">Male</option>
							<option value="Female">Female</option>
						</select> <br /> <br /> <input type="submit" value="Register">
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>