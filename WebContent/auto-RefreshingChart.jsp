<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Auto-RefreshingChart</title>
<script type="text/javascript">
	function refreshPage() {
	    document.forms.formId.submit();
    }
</script>
</head>
<body>
	<h3>Create Pie Chart Dynamically using JFreechart</h3>
	<!--
In Jsp, servlet is called via img tag’s src attribute, this triggers the servlet and render the image on jsp.
To automate the page refreshing process, the following code is used

It will reload the current request after the given amount of seconds, as if you’re pressing F5. Here I set the time
interval to 10 seconds, So the page will refresh and render updated data from database for every 10 seconds,
this value can be set based on your requirement.

 
Note :
This automatic refreshing can cause a considerable performance impact since it hits database for every 10 second,
so it is a good practice to have a ‘Refresh’ button to enable the end-users to decide on when to refresh the page. 
	 -->
	<%response.setIntHeader("Refresh", 10);%>
	<form id="formId" action="AutoRefreshChartServlet">
		<input type="button" onclick="refreshPage()" value="Refresh Page" />
		<br /> <img src="AutoRefreshChartServlet" />
	</form>
</body>
</html>