<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Autocomplete in java web application using Jquery and JSON</title>

<script src="js/jquery/jquery-1.11.2.min.js" type="text/javascript"></script>
<script src="js/jquery/jquery-ui.min.js" type="text/javascript"></script>
<script src="js/autocompleter.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" media="screen" href="css/jquery-ui.min.css" />
<!-- User defied css -->
<link rel="stylesheet" type="text/css" href="css/styleAutocompleter.css">

</head>
<body>
	<div class="header"><h3>Autocomplete in java web application using Jquery and JSON</h3></div>
	<br />
	<div class="search-container">
		<div class="ui-widget">
			<input type="text" id="search" name="search" class="search" />
		</div>
	</div>
</body>
</html>