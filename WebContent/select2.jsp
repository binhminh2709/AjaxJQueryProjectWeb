<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>select2</title>
<script type="text/javascript" src="js/jquery/jquery-1.11.2.min.js" /></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
</head>
<body>
<script type="text/javascript">
  $(".js-data-example-ajax").select2({
    ajax: {
      url: "https://api.github.com/search/repositories",
      dataType: 'json',
      delay: 250,
      data: function (params) {
        return {
          q: params.term, // search term
          page: params.page
        };
      },
      processResults: function (data, page) {
        // parse the results into the format expected by Select2.
        // since we are using custom formatting functions we do not need to
        // alter the remote JSON data
        return {
          results: data.items
        };
      },
      cache: true
    },
    escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
    minimumInputLength: 1,
    templateResult: formatRepo, // omitted for brevity, see the source of this page
    templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
  });
      
  function formatRepo(repo) {
    if (repo.loading) {
      return repo.text;
    }
    var markup = '<div class="clearfix">' +
      '<div class="col-sm-1">' +
      '<img src="' + repo.owner.avatar_url + '" style="max-width: 100%" />' +
      '</div>' +
      '<div clas="col-sm-10">' +
      '<div class="clearfix">' +
      '<div class="col-sm-6">' + repo.full_name + '</div>' +
      '<div class="col-sm-3"><i class="fa fa-code-fork"></i> ' + repo.forks_count + '</div>' +
      '<div class="col-sm-2"><i class="fa fa-star"></i> ' + repo.stargazers_count + '</div>' +
      '</div>';
    
    if (repo.description) {
      markup += '<div>' + repo.description + '</div>';
    }
    markup += '</div></div>';
    return markup;
  }

  function formatRepoSelection(repo) {
    return repo.full_name || repo.text;
  }
</script>

<select class="js-data-example-ajax">
  <option value="3620194" selected="selected">select2/select2</option>
  <option value="11">DAI LY A</option>
</select>

</body>
</html>